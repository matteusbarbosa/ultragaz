<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'UserController@index');
Route::get('/recover', 'UserController@recover');
Route::get('/recover-change', 'UserController@change');
Route::resource('/poll', 'PollController');
Route::resource('/mural', 'MuralController');
Route::get('/user/{id}/json', 'UserController@showJson');
Route::get('/user/search', 'UserController@search');
Route::resource('/user', 'UserController');
Route::resource('/question', 'QuestionController');
Route::resource('/notification', 'NotificationController');

Route::get('/question/{id}/json', 'QuestionController@showJson');


Route::get('/', function () {
    return view('welcome');
});
