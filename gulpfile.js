'use strict';

var gulp = require('gulp');

var sass = require('gulp-sass');

var gutil = require('gulp-util');

var ftp = require('gulp-ftp');

var sourcemaps = require('gulp-sourcemaps');

const errorHandler = require('gulp-error-handle');

const logError = function(err) {
  gutil.log(err);
  this.emit('end');
};

gulp.task('sass_login', function () {
  return gulp.src('public/assets/css/sass/login.scss')
  .pipe(sass({outputStyle: 'compressed'})
  .on('error', sass.logError))
  .pipe(sourcemaps.init())
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest('public/assets/css'));
});

gulp.task('sass_poll', function () {
  return gulp.src('public/assets/css/sass/poll.scss')
  .pipe(sass({outputStyle: 'compressed'})
  .on('error', sass.logError))
  .pipe(sourcemaps.init())
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest('public/assets/css'));
});

gulp.task('sass_question', function () {
  return gulp.src('public/assets/css/sass/question.scss')
  .pipe(sass({outputStyle: 'compressed'})
  .on('error', sass.logError))
  .pipe(sourcemaps.init())
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest('public/assets/css'));
});

gulp.task('sass_notification', function () {
  return gulp.src('public/assets/css/sass/notification.scss')
  .pipe(sass({outputStyle: 'compressed'})
  .on('error', sass.logError))
  .pipe(sourcemaps.init())
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest('public/assets/css'));
});

gulp.task('watch', function () {
  gulp.watch('public/assets/css/sass/login.scss', ['sass_login']);
  gulp.watch('public/assets/css/sass/poll.scss', ['sass_poll']);
  gulp.watch('public/assets/css/sass/question.scss', ['sass_question']);
  gulp.watch('public/assets/css/sass/notification.scss', ['sass_notification']);
});

 gulp.task('ftp-deploy-production', function () {
  var remotePath = 'public_html/poep_basic/';
    return gulp.src(['./wp-content/themes/12042017/**/*'])
        .pipe(ftp({
            host: '',
            user: '',
            pass: '',
            remotePath : remotePath
        }))
        .pipe(errorHandler(logError))
        // you need to have some kind of stream after gulp-ftp to make sure it's flushed
        // this can be a gulp plugin, gulp.dest, or any kind of stream
        // here we use a passthrough stream
        .pipe(gutil.noop());
});

gulp.task('default', [ 'watch' ]);
