# Entrega de telas
ROTA                    	BLADE               TELA
/login                      login               Entrada
/recover                    recover             Solicitar Recuperar Senha
/recover-change             reover_change       Mudar senha após e-mail
/poll                   	polls               Lista de Enquetes
/poll/create            	poll_form           Add/Edit Enquete
/mural                  	mural               Mural de questões
/question/1             	question_form       Área de resposta do mural
/notification            	notifications       Lista de Notificações
/notification/create        notification_form   Add/Edit Notificação