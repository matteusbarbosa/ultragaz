<!-- Stored in resources/views/child.blade.php -->

@extends('layout.wide')

@section('title', trans('object.mural'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/question.css') }}" defer>
@endsection

@section('header')
<script>
    $(document).ready( function () {
        $('table#list').DataTable({
            "language":{
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                }
            },
            "columnDefs": [
            { "orderable": false, "targets":5 }
            ]
        });
    } );
</script>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')

<form method="POST">
    <div class="container" >
        <div class="row card border-secondary">
            <div class="card-header">Mural</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ trans('legend.subject-type') }}</label>
                            <select class="form-control" name="subject-type" placeholder="" type="email">
                                <option></option>
                            </select>
                            
                        </div>
                        <div class="form-group">
                            <label for="field-user">{{ trans('object.user') }}</label>
                            <input id="field-user" class="form-control" placeholder="" type="text">
                        </div>
                        
                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <div class="form-group">
                            <label for="field-status">{{ trans('legend.status') }}</label>
                            <select id="field-status" class="form-control" name="status" placeholder="">
                            </select>
                            
                        </div>
                        
                        
                    </div>
                </div>
                <div class="row d-flex justify-content-start">
                    <div class="form-group">
                        <button type="button" data-id="" class="btn btn-success btn-search float-right">{{ trans('legend.search')}}</button>
                    </div>
                </div>
            </div>
            
            
            
            
        </div>
        
    </div>
    
    
    
</form>
<div class="table-responsive">
    <table id="list" class="display">
        <thead>
            <tr>
                <th>{{ trans('legend.subject')}}</th>
                <th>{{ trans('legend.type')}}</th>
                <th>{{ trans('object.user')}}</th>
                <th>{{ trans('legend.message-last')}}</th>
                <th>{{ trans('legend.days')}}</th>
                <th>{{ trans('legend.status')}}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Row 1 Data 1</td>
                <td>Row 1 Data 2</td>
                <td><a href="#" class="toggle-modal-user" data-id="1">Row 1 Data 2 <i class="fa fa-user-o"></i></a></td>
                <td>Row 1 Data 2</td>
                <td>Row 1 Data 2</td>
                <td>
                    <ul class="list-inline">
                        <li class="list-inline-item"><button type="button" data-id="1" class="btn btn-secondary toggle-modal-question"><i class="fa fa-eye"></i> {{ trans('legend.view')}}</button></li>
                            <li class="list-inline-item"><a href="/question/1" type="button" class="btn btn-success"><i class="fa fa-mail-reply"></i> {{ trans('legend.question-answer')}}</a></li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </div>
        
        
        
        <!-- The Modal -->
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modal Heading</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <!-- Modal body -->
                    <div class="modal-body">
                        Modal body..
                    </div>
                    
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <script>
            
            $(document).ready(function(){
                //nova pergunta (ler data-target 1 ou 2)
                $('.toggle-modal-user').on('click', function(){
                    var user_id = $(this).data('id');
                    
                    
                    //ajax http://local.ultragaz.com
                    
                    $.ajax({
                        url: "/user/1/json"
                    }).done(function(result) {
                        $('#modal').find('.modal-title').text(result.name);
                        $('#modal').find('.modal-body').text(result.name);
                        $('#modal').modal('show');
                    });
                    
                    
                });

                  $('.toggle-modal-question').on('click', function(){
                    var user_id = $(this).data('id');
                    //ajax http://local.ultragaz.com
                    
                    $.ajax({
                        url: "/question/1/json"
                    }).done(function(result) {
                        $('#modal').find('.modal-title').text(result.title);
                        $('#modal').find('.modal-body').text(result.details);
                        $('#modal').modal('show');
                    });
                });
                
                
            });
            
            
        </script>
        @endsection