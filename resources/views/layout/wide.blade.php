<html>
<head>
<title>{{ config('app.name') }} - @yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cerulean.min.css') }}" defer>
<link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!-- BOOTSTRAP -->
<script
src="https://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous" defer></script>

<link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">

<!-- DATATABLES -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" defer>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@section('header')
@show
</head>
<body class="layout-wide">
@section('sidebar')
@show
@include('partial.navbar_top')
@include('partial.breadcrumb')
<div class="container">
@yield('content')
</div>
</body>
</html>