<!-- Stored in resources/views/child.blade.php -->

@extends('layout.wide')

@section('title', trans('legend.poll-list'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/poll.css') }}" defer>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')
<form method="POST">
@csrf
<div class="container" >
<div class="card border-secondary">
<div class="card-header">{{ trans('legend.poll-new') }}</div>
<div class="card-body">
    <div class="row">
<div class="col-lg-6">
<div class="form-group">
<div class="alert alert-dismissible alert-danger">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong>Erro.</strong> 
</div>
</div>
<div class="form-group">
<label for="exampleInputEmail1">Título</label>
<input class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" type="email">

</div>
<div class="form-group">
<label>Descrição</label>
<input class="form-control" placeholder="" type="text">
</div>
</div>
<div class="col-lg-5 offset-lg-1">

<div class="form-group">

<button class="btn btn-secondary btn-new-question btn-sm" data-target="#question-type-1" type="button" >+ Nova pergunta Múltipla Escolha</button>

</div>

<div class="form-group">

<button class="btn btn-secondary btn-new-question btn-sm" data-target="#question-type-2" type="button">+ Nova pergunta Dissertativa</button></div>

</div>

</div>
<div class="row">
    <div class="col-lg-12 d-flex justify-content-end">
    <button class="btn form-text btn-success btn-save" type="submit">Salvar</button>


</div>
</div>
</div>

<div class="box-option-model d-none">
<div class="form-group box-option">
        <label>Opção <span class="option-number"></span></label>
        <div class="input-group">
                
                <input name="question[][options]" class="form-control field-option" placeholder="" type="text">
                <div class="wrap-btn-question input-group-append">
                <button class="btn btn-sm form-text btn-secondary btn-delete-option" type="button"><i class="fa fa-minus"></i></button>
                </div>
              </div>



</div>
</div>


<div class="row d-none box-question-model" id="question-type-1">

<div class="card box-question">    
<input type="hidden" class="question-id" value=""> 
<div class="card-header">
<a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
<span class="card-title"><span class="question-number"></span>. Pergunta de Múltipla Escolha </span>
</a>
<button class="btn btn-secondary btn-delete-question float-right btn-sm">Excluir</button>

</div>
<div id="collapseOne" class="collapse show" data-parent="#list">
<div class="card-body">


<div class="form-group">
<label for="exampleInputEmail1">{{ trans('object.question') }}</label>
<textarea name="question[]" class="form-control" placeholder=""></textarea>

</div>
<label>{{ trans('legend.options') }}</label>
<hr>
<ul class="list-options">

</ul>
<div class="wrap-btn-question">
<button class="btn btn-sm form-text btn-secondary btn-new-option" type="button"><i class="fa fa-plus"></i></button>
</div>

</div>
</div>
</div>
</div>


<div class="row d-none box-question-model" id="question-type-2">

<div class="card box-question">     
<input type="hidden" class="question-id" value="">
<div class="card-header">
<a class="card-link" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
<span class="card-title"><span class="question-number"></span>. Pergunta Dissertativa </span>
</a>
<button type="button" class="btn btn-secondary btn-delete-question float-right btn-sm">Excluir</button>

</div>
<div id="collapseOne" class="collapse show" data-parent="#list">
<div class="card-body">


<div class="form-group">
<label for="exampleInputEmail1">{{ trans('object.question') }}</label>
<textarea name="question[]" class="form-control" placeholder=""></textarea>

</div>

</div>
</div>
</div>
</div>


<div id="list">



</div>

</div>
</div>
</form>

<!-- The Modal -->
<div class="modal fade" id="modal-delete">
<div class="modal-dialog">
<div class="modal-content">

<!-- Modal Header -->
<div class="modal-header">
<h4 class="modal-title">{{ trans('legend.poll-delete') }}</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<!-- Modal body -->
<div class="modal-body">
{{ trans('legend.are-you-sure') }}
</div>

<!-- Modal footer -->
<div class="modal-footer">
<button type="button" class="btn btn-danger btn-delete-confirm" data-dismiss="modal">Confirmar</button>
</div>

</div>
</div>
</div>

<script>

$(document).ready(function(){
    //nova pergunta (ler data-target 1 ou 2)
    $('.btn-new-question').on('click', function(){
        var target = $(this).data('target');
        var question_new = $(target).clone();
        var item_number = $('#list .card').length;
        var card_link = question_new.find('.card-link');
        card_link.attr('href', '#collapse-'+item_number);
        var collapse = question_new.find('.collapse');
        collapse.attr('id', 'collapse-'+item_number);
        question_new.find('.question-id').val(item_number);
        question_new.find('.question-number').text(item_number+1);
        question_new.removeClass('d-none').addClass('box-question');
        $('#list').append(question_new.html());
        $('#list').find('.box-question:last').find('[name="question[]"]').focus();
        
        
    });
    
    $('#list').on('click','.btn-new-option', function(){
        var option_new = $('.box-option-model').clone();
        var question_parent = $(this).parents('.box-question');
        var question_id = question_parent.find('.question-id').first().val();
        var list_options = question_parent.find('.list-options');
        var item_number = list_options.find('li').length + 1;
        option_new.find('.field-option').attr('name', 'question['+question_id+'][options]');
        option_new.find('.option-number').text(item_number);
        list_options.append('<li>'+option_new.html()+'</div>');
        
        
        if(list_options.find('li').length == 0)
        $(this).siblings('.btn-delete-option').addClass('d-none');
        else
        $(this).siblings('.btn-delete-option').removeClass('d-none');
        
    });
    
    
    $('#list').on('click','.btn-delete-option', function(e){
        var question_parent = $(this).parents('.box-question');
        var list_options = question_parent.find('.list-options');
        $(this).parents('li').remove();

        optionNumbersUpdate(question_parent);
        
        if(list_options.find('li').length == 0)
        $(this).addClass('d-none')
        else
        $(this).removeClass('d-none');
        
    });
    
    function questionNumbersUpdate(){
        
        $('#list .box-question').each(function(i){
            var question = $(this);
            var card_link = question.find('.card-link');
            card_link.attr('href', '#collapse-'+i);
            var collapse = question.find('.collapse');
            collapse.attr('id', 'collapse-'+i);
            question.find('.question-id').val(i);
            question.find('.question-number').text(i+1);
            
            $(this).find('.question-number').text(i+1);
        });
        
    }

    function optionNumbersUpdate(box_question){
 
        $(box_question).find('.list-options li').each(function(i){
 
            $(this).find('.option-number').text(i+1);
        });
    }
    
    
    $('#modal-delete').on('click','.btn-delete-confirm', function(){
        var id = $(this).data('id');        
        var token = document.querySelector("meta[name='csrf-token']").getAttribute("content");
        
        question_parent.remove();
        questionNumbersUpdate();
        var question_parent = $(this).parents('.box-question');
        
        $.ajax({
            url: "/poll/",
            data: {
                "id": id,
                "_method": 'DELETE',
                "_token": token,
            },
        }).done(function(result) {
            //confirma remoção do banco
            
        });
    });
    
    $('#list').on('click','.btn-delete-question', function(){
        var id = $(this).data('id');
        $('.btn-delete-confirm').data('id', id);
        $('#modal-delete').modal('show');
    });
    
    
    
});


</script>


@endsection