<!-- Stored in resources/views/child.blade.php -->

@extends('layout.wide')

@section('title', trans('legend.notification-list'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/notification.css') }}" defer>
@endsection

@section('header')
    <script>
    $(document).ready( function () {
    $('table#list').DataTable({
        "language":{
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "Pesquisar",
    "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
    },
    "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
    }
},
        "columnDefs": [
    { "orderable": false, "targets": 6 }
  ]
    });
} );
    </script>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')
<div class="card card-notification border-secondary">
            <div class="card-header">{{ trans('legend.notification-list') }}             
                    <a href="{{ url('/notification/create') }}" class="btn btn-info btn-add float-right" type="button"><i class="fa fa-plus"></i> {{ trans('legend.notification-new') }}</a>
            </div>
            
            <div class="card-body">
<div class="table-responsive">
    <table id="list" class="display">
    <thead>
        <tr>
            <th>ID</th>
            <th>{{ trans('legend.title') }}</th>
            <th>{{ trans('legend.message') }}</th>
            <th>{{ trans('legend.send-at') }}</th>
            <th>{{ trans('legend.user-sent') }}</th>
            <th>{{ trans('legend.status') }}</th>
            <th>{{ trans('legend.actions') }}</th>
           
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Row 1 Data 1</td>
            <td>Row 1 Data 2</td>
            <td>Row 1 Data 2</td>
            <td>Row 1 Data 2</td>
            <td>Row 1 Data 2</td>
            <td class="text-center"><span class="badge badge-danger">{{ trans('legend.canceled') }}</span><span class="badge badge-success">{{ trans('legend.finished') }}</span></td>
            <td>
                    <div class="dropdown dropdown-notification">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-external-link"></i> {{ trans('legend.options') }}
                            </button>
                            <div class="dropdown-menu">
                              <a class="dropdown-item" href="{{ url('/notification/1') }}"><i class="fa fa-external-link"></i> {{ trans('legend.view') }}</a>
                              <a class="dropdown-item" href="{{ url('/notification/1') }}"><i class="fa fa-external-link"></i> {{ trans('legend.edit') }}</a>
                              <a class="dropdown-item" href="{{ url('/notification/push/1') }}"><i class="fa fa-play"></i> {{ trans('legend.push-send') }}</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="{{ url('/notification/delete/1') }}"><i class="fa fa-times"></i> {{ trans('legend.cancel') }}</a>
                            </div>
                          </div>
            </td>
        </tr>
    </tbody>
</table>
</div>
</div>
</div>

@endsection