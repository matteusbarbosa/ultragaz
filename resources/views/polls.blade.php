<!-- Stored in resources/views/child.blade.php -->

@extends('layout.wide')

@section('title', trans('legend.poll-list'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/poll.css') }}" defer>
@endsection

@section('header')
<script>
$(document).ready( function () {
    $('table#list').DataTable({
        "language":{
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        },
        paging: true,
        searching: false,
        "columnDefs": [
            { "orderable": false, "targets": 6 }
            ]
        });
    } );
    </script>
    @endsection
    
    @section('sidebar')
    @parent
    @endsection
    
    @section('content')
    <div class="table-responsive">
    <table id="list" class="display">
    <thead>
    <tr>
    <th>{{ trans('object.poll')}}</th>
    <th>{{ trans('legend.questions')}}</th>
    <th>{{ trans('legend.participants')}}</th>
    <th>{{ trans('legend.status')}}</th>
    <th>{{ trans('legend.start-at')}}</th>
    <th>{{ trans('legend.end-at')}}</th>
    <th>
    <a href="/poll/create/" class="btn btn-secondary">{{ trans('legend.poll-new')}}</a></th>
    </tr>
    </thead>
    <tbody>
    @for ($i = 0; $i < 100; $i++)
    <tr>
    <td>Row 1 Data 1</td>
    <td>3</td>
    <td>50</td>
    <td>Row 1 Data 2</td>
    <td>Row 1 Data 2</td>
    <td>Row 1 Data 2</td>
    <td>
    <ul class="list-inline">
    <li class="list-inline-item"><a href="/poll/close/" class="btn btn-secondary d-none">{{ trans('legend.close')}}</a></li>
    <li class="list-inline-item"><a href="/poll/release/" class="btn btn-secondary">{{ trans('legend.release')}}</a></li>
    <li class="list-inline-item"><a href="/poll/edit/" class="btn btn-secondary">{{ trans('legend.edit')}}</a></li>
    <li class="list-inline-item"><a href="#" data-id="1" class="btn btn-delete btn-secondary">{{ trans('legend.delete')}}</a></li>
    </ul>
    </td>
    </tr>
    @endfor
    </tbody>
    </table>
    </div>
    
    <!-- The Modal -->
    <div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
    <div class="modal-content">
    
    <!-- Modal Header -->
    <div class="modal-header">
    <h4 class="modal-title">{{ trans('legend.poll-delete') }}</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    
    <!-- Modal body -->
    <div class="modal-body">
{{ trans('legend.are-you-sure') }}
    </div>
    
    <!-- Modal footer -->
    <div class="modal-footer">
    <button type="button" class="btn btn-danger btn-delete-confirm" data-dismiss="modal">Confirmar</button>
    </div>
    
    </div>
    </div>
    </div>
    
    <script>
    
    $(document).ready(function(){

        $('.btn-delete-confirm').on('click', function () {
            var id = $(this).data('id');

            var token = document.querySelector("meta[name='csrf-token']").getAttribute("content");

            $.ajax({
                url: "/poll/",
            data: {
                "id": id,
                "_method": 'DELETE',
                "_token": token,
            },
                    }).done(function(result) {
                      alert('ok');
                    });
        });
        
        $('.btn-delete').on('click', function () {
            var id = $(this).data('id');
            $('.btn-delete-confirm').data('id', id);
            $('#modal-delete').modal('show');
        });
        
    });
    </script>
    
    @endsection