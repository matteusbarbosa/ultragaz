@extends('layout.wide')

@section('title', trans('legend.recover-change'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/login.css') }}" defer>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    
                    <!-- form card login -->
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="mb-0">{{ trans('legend.recover-change') }}</h3>
                        </div>
                        <div class="card-body">
                            <form class="form" role="form" method="POST">
                                @csrf
                                <div class="form-group">
                                    <div class="alert alert-dismissible alert-success">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Sucesso.</strong> 
                                    </div>
                                    <div class="alert alert-dismissible alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Erro.</strong>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="uname1">Usuário</label>
                                    <input type="text" class="form-control form-control-lg" value="Nome do usuario" disabled name="uname1" id="uname1" required="">
                                    <div class="invalid-feedback">{{ trans('legend.field-fix') }}</div>
                                </div>
                                <div class="form-group">
                                    <label>Senha</label>
                                    <input type="password" class="form-control form-control-lg" id="pwd1" required="" autocomplete="new-password">
                                    <div class="invalid-feedback">{{ trans('legend.field-fix') }}</div>
                                </div>
                                <div class="form-group">
                                    <label>Repita a Senha</label>
                                    <input type="password" class="form-control form-control-lg" id="pwd1" required="" autocomplete="new-password">
                                    <div class="invalid-feedback">{{ trans('legend.field-fix') }}</div>
                                </div>
                                <div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Confirmar</button>
                            </form>
                        </div>
                        <!--/card-block-->
                    </div>
                    <!-- /form card login -->
                    
                </div>
                
                
            </div>
            <!--/row-->
            
        </div>
        <!--/col-->
    </div>
    <!--/row-->
</div>
<!--/container-->

<script>
    
    $(document).ready(function(){
        
        
        
    });
    
    
</script>


@endsection