@extends('layout.wide')

@section('title', trans('legend.question-new'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/question.css') }}" defer>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')
<form method="POST">
    @csrf
    <div class="container" >
        <div class="card card-question border-secondary">
            <div class="card-header">{{ trans('object.mural') }}             
                <button class="btn btn-info btn-export float-right" type="button">{{ trans('legend.question-export') }}</button>
                <button class="btn btn-success btn-restore float-right" type="button">{{ trans('legend.question-restore') }}</button>
            </div>
            <div class="card-body">
                    <div class="form-group">
                            <div class="alert alert-dismissible alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Erro.</strong>
                            </div>
                        </div>
                <div class="row">
                    <div class="col-lg-6">
                     
                        <div class="form-group">
                            <h5 for="info-user">{{ trans('object.user') }} </h5>
                            <p id="info-user">Lorem Ipsum <i class="fa fa-user-o"></i></p>
                            
                        </div>
                        <div class="form-group">
                            <h5 >{{ trans('legend.talk-title') }}</h5>
                            <p >Lorem Ipsum</p>                       
                        </div>
                        
                    </div>
                    <div class="col-lg-5 offset-lg-1">
                        <div class="form-group">
                            <h5>{{ trans('legend.status') }}</h5>
                            <p>Lorem Ipsum</p>  
                            
                        </div>
                        
                        <div class="form-group">
                            <h5>{{ trans('legend.subject') }}</h5>
                            <p>Lorem Ipsum</p>                       
                        </div>
                        
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <h5>{{ trans('legend.you') }}</h5>
                            <textarea name="message" placeholder="{{ trans('legend.answer-write') }}" class="form-control" placeholder=""></textarea>
                            
                            
                        </div>
                        <div class="form-group">
                            <ul class="list-inline">
                                <li class="list-inline-item"><button type="submit" class="btn btn-success">{{ trans('legend.send')}}</button></li>
                                <li class="list-inline-item"><button type="button" data-id="1" type="button" class="btn btn-danger">{{ trans('legend.cancel')}}</button></li>
                                <li class="list-inline-item"><button type="button" data-id="1" type="button" class="btn btn-warning">{{ trans('legend.talk-close')}}</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                
                <div class="row box-message">
                    <div class="col-lg-3 text-center">
                        <i class="fa fa-check-square"></i>
                        
                    </div>
                    <div class="col-lg-6">
                        <h5>{{ trans('legend.subject') }}</h5>
                        <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
                        <br>
                        <p> <i class="fa fa-paper-plane-o"></i> {{ trans('legend.sent-at', ['timestamp' => date('d/m/Y H:i:s', strtotime(now())), 'days' => 55]) }}</p>
                        <p> <i class="fa fa-eye"></i>{{ trans('legend.read-at', ['timestamp' => date('d/m/Y H:i:s', strtotime(now())), 'days' => 10]) }}</p>
                        
                    </div>
                </div>
                
                <div class="row box-message">
                    <div class="col-lg-3 text-center">
                        <i class="fa fa-check-square"></i>
                        
                    </div>
                    <div class="col-lg-6">
                        <h5>{{ trans('legend.subject') }}</h5>
                        <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.</p>
                        <br>
                        <p> <i class="fa fa-paper-plane-o"></i> {{ trans('legend.sent-at', ['timestamp' => date('d/m/Y H:i:s', strtotime(now())), 'days' => 55]) }}</p>
                        <p> <i class="fa fa-eye"></i>{{ trans('legend.read-at', ['timestamp' => date('d/m/Y H:i:s', strtotime(now())), 'days' => 10]) }}</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    
    $(document).ready(function(){
        
        
        
    });
    
    
</script>


@endsection