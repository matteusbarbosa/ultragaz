<div class="bs-component">
  <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-top">
    <a class="navbar-brand" href="/poll">Ultragaz</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarColor03">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <div class="dropdown">
            <button class="btn btn-link dropdown-toggle" type="button" id="menu-poll" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Enquetes
            </button>
            <div class="dropdown-menu" aria-labelledby="menu-poll">
              <a class="dropdown-item" href="/poll">Lista</a>
              <a class="dropdown-item" href="/poll/create">Nova</a>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <div class="dropdown">
            <button class="btn btn-link dropdown-toggle" type="button" id="menu-mural" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Mural
            </button>
            <div class="dropdown-menu" aria-labelledby="menu-mural">
              <a class="dropdown-item" href="/mural">Lista</a>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <div class="dropdown">
            <button class="btn btn-link dropdown-toggle" type="button" id="menu-notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Notificações
            </button>
            <div class="dropdown-menu" aria-labelledby="menu-notification">
              <a class="dropdown-item" href="/notification">Lista</a>
            </div>
          </div>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <ul>
          <li class="nav-item">
            <a class="nav-link" href="/logout">Sair</a>
          </li>
        </ul>
      </form>
    </div>
  </nav>
</div>