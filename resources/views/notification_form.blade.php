<!-- Stored in resources/views/child.blade.php -->

@extends('layout.wide')

@section('title', trans('legend.notification-list'))

@section('header')
@parent
<script
src="https://code.jquery.com/jquery-1.12.4.min.js"
integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
crossorigin="anonymous"></script>
<script src="{{ asset('assets/js/tag.js') }}" type="text/javascript" charset="utf-8"></script>
<link href="{{ asset('assets/css/tag.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/notification.css') }}" defer>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')
<form method="POST">
    @csrf
    <div class="container" >
        <div class="card border-secondary card-notification">
            <div class="card-header">{{ trans('legend.notification-new') }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <div class="alert alert-dismissible alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Erro.</strong>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Título</label>
                            <input class="form-control" aria-describedby="" placeholder="" type="text">
                        </div>
                        <div class="form-group">
                            <label>Mensagem</label>
                            <textarea class="form-control" aria-describedby="" placeholder=""></textarea>
                            
                        </div>
                        <h4>Enviar para</h4>
                        <div class="form-group row">
                            <div class="col-4"><label for="send-all" class="label-left">Todos</label></div>
                            <div class="col-6"><input id="send-all" name="send_all" placeholder="" type="checkbox"></div>
                            
                            
                        </div>
                        <div class="form-group box-select-filters">
                            <label>Usuários (App)</label>
                            
                            <input type="text" id="users" width="100%" name="users">
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <h4>Área de destino</h4>
                        <div class="form-group row">
                            <div class="col-4"><label for="field-target-area">Tipo</label></div>
                            <div class="col-6">
                                <select id="select-type" class="form-control" name="select_type">
                                    <option value="">Selecione</option>
                                    <option value="user">Usuário</option>
                                    <option value="link">Link</option>
                                    <option value="product">Produto</option>
                                    <option value="communication">Comunicação</option>
                                    <option value="news">Notícia</option>
                                    <option value="tech">Tecnologia</option>
                                    <option value="trend">Tendência</option>
                                </select>
                            </div>
                            
                            
                        </div>
                        
                        <div class="form-group row d-none" id="box-select-items">
                            <div class="col-4"> <label>Itens</label></div>
                            <div class="col-6">
                                <input type="text" id="area-items" class="form-control" width="100%" name="area_items">
                                <div style="clear:both;"></div>
                            </div>                       
                            
                            
                        </div>
                    </div>
                    
                    
                    
                    
                </div>
                
                
                <div id="list">
                    
                    
                    
                </div>
                
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end">
            <a href="/notification/cancel/" class="btn btn-secondary"><i class="fa fa-times"></i> {{ trans('legend.cancel')}}</a>
            <a href="/notification/save/" class="btn btn-success"><i class="fa fa-check"></i> {{ trans('legend.save')}}</a>
        </div>
    </div>
</form>

<script>
    
    
    $(document).ready(function() {
        
        var ms1 = $('#users').tagSuggest({
            //json
            data: "/user/search/",
            sortOrder: 'name',
            maxDropHeight: 200,
            name: 'users'
        });
        
        $('#send-all').on('click', function(e){
            var el = $(e.target);
            
            if(el.is(':checked'))
            $('.box-select-filters').addClass('d-none');
            else
            $('.box-select-filters').removeClass('d-none');
            
        });
        
        var select_type = fetchFilter();
        
        $('#select-type').on('change', function(e){
            $('#box-select-items').removeClass('d-none');
            var area = $(e.target).find('option:selected').val();
            select_type.combobox.empty();
            select_type.setData("/"+area+"/search/");
            
        });
        
        $('#area-items').on('change', function(e){
            var area = $('#select-type').find('option:selected').val();
            select_type.combobox.empty();
            select_type.setData("/"+area+"/search/");
        });
        
        
        function fetchFilter(area){
            return $('#area-items').tagSuggest({
                //json ?query=find_term
                data: "/"+area+"/search/",
                sortOrder: 'name',
                maxDropHeight: 200,
                name: 'area_items'
            });
            
            
        }
        
        
        
        
        
    });
    
</script>

@endsection