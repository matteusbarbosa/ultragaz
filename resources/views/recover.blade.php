@extends('layout.wide')

@section('title', trans('legend.user-recover'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/login.css') }}" defer>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center text-white mb-4">Bootstrap 4 Login Form</h2>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    
                    <!-- form card login -->
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="mb-0">{{ trans('legend.user-recover') }}</h3>
                        </div>
                        <div class="card-body">
                            <form class="form" role="form" autocomplete="off" id="formLogin" novalidate="" method="POST">
                                @csrf
                                <div class="form-group">
                                    <div class="alert alert-dismissible alert-success">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Sucesso.</strong> 
                                    </div>
                                    <div class="alert alert-dismissible alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Erro.</strong>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="uname1">E-mail</label>
                                    <input type="email" class="form-control form-control-lg rounded-0" name="uname1" id="uname1" required="">
                                    <div class="invalid-feedback">Oops, you missed this one.</div>
                                </div>
                                <div>
                                    <label class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Enviar</button>
                            </form>
                        </div>
                        <!--/card-block-->
                    </div>
                    <!-- /form card login -->
                    
                </div>
                
                
            </div>
            <!--/row-->
            
        </div>
        <!--/col-->
    </div>
    <!--/row-->
</div>
<!--/container-->

<script>
    
    $(document).ready(function(){
        
        
        
    });
    
    
</script>


@endsection