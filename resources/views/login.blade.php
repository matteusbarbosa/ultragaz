@extends('layout.wide')

@section('title', trans('legend.user-login'))

@section('header')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/login.css') }}" defer>
@endsection

@section('sidebar')
@parent
@endsection

@section('content')
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            <h2 class="text-center text-white mb-4">Bootstrap 4 Login Form</h2>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    
                    <!-- form card login -->
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="mb-0">Login</h3>
                        </div>
                        <div class="card-body">
                            <form class="form" role="form" method="POST">
                                @csrf
                                <div class="form-group">
                                    <div class="alert alert-dismissible alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Erro.</strong>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="uname1">Usuário</label>
                                    <input type="text" class="form-control form-control-lg" name="uname1" id="uname1" required="">
                                    <div class="invalid-feedback">{{ trans('legend.field-fix') }}</div>
                                </div>
                                <div class="form-group">
                                    <label>Senha</label>
                                    <input type="password" class="form-control form-control-lg" id="pwd1" required="">
                                    <div class="invalid-feedback">{{ trans('legend.field-fix') }}</div>
                                </div>
                                <div>
                                    <label class="custom-checkbox">
                                        <input type="checkbox" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <a href="/recover" class="custom-control-description small text-dark">Esqueci minha senha</a>
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Login</button>
                            </form>
                        </div>
                        <!--/card-block-->
                    </div>
                    <!-- /form card login -->
                    
                </div>
                
                
            </div>
            <!--/row-->
            
        </div>
        <!--/col-->
    </div>
    <!--/row-->
</div>
<!--/container-->


@endsection