<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Pagination Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used by the paginator library to build
  | the simple pagination links. You are free to change them to anything
  | you want to customize your views to better match your application.

  */

  'actions' => 'Ações',
  'are-you-sure' => 'Tem certeza disso?',
  'answer-write' => 'Escreva uma resposta',
  'cancel' => 'Cancelar',
  'canceled' => 'Cancelado',
  'status' => 'Status',
  'start-at' => 'Data de Início',
  'days' => 'Dias',  
  'delete' => 'Excluir',  
  'delete-confirm' => 'Tem certeza de que deseja excluir esse registro?',  
  'edit' => 'Editar',  
  'end-at' => 'Data de Término',  
  'filter' => 'Filtro',  
  'finished' => 'Finalizado',  
  'field-fix' => 'Corrija este campo',
  'poll-list' => 'Enquetes – Listar',  
  'poll-new' => 'Nova Enquete',  
  'poll-close' => 'Finalizar',  
  'poll-release' => 'Liberar',  
  'poll-edit' => 'Editar',  
  'poll-delete' => 'Excluir enquete',  
  'send' => 'Enviar',  
  'subject' => 'Assunto',  
  'subject-type' => 'Tipo do assunto',  
  'type' => 'Tipo',  
  'message' => 'Mensagem',  
  'message-last' => 'Última mensagem',  
  'notification-new' => 'Nova notificação',  
  'notification-list' => 'Notificações',  
  'read-at' => 'lida em :timestamp - :days dias atrás',  
  'participants' => 'Participantes',  
  'push-send' => 'Enviar push',  
  'question-answer' => 'Responder',  
  'question-restore' => 'Reabrir Ticket',  
  'question-export' => 'Exportar conversas para PDF',  
  'questions' => 'Questões',  
  'options' => 'Opções',  
  'recover-change' => 'Mudar senha',  
  'release' => 'Liberar',  
  'save' => 'Salvar',  
  'search' => 'Pesquisar',  
  'send-at' => 'Data de envio',  
  'sent-at' => 'enviada em :timestamp - :days dias atrás',  
  'status' => 'Status',  
  'talk-title' => 'Nome da Conversa',  
  'talk-close' => 'Encerrar Conversa',  
  'title' => 'Título',  
  'user-sent' => 'Quem enviou',  
  'user-login' => 'Entrar',  
  'user-recover' => 'Recuperar senha',  
  'view' => 'Visualizar',  
  'you' => 'Você',  

];
